from django.shortcuts import render, HttpResponse
import requests
import time
import re
import json


# Create your views here.

def ticket(html):
    '''解析凭证'''
    from bs4 import BeautifulSoup
    ret = {}
    soup = BeautifulSoup(html, 'html.parser')
    for tag in soup.find(name='error').find_all():
        ret[tag.name] = tag.text
    return ret


def login(req):
    '''user QRcode Login  '''
    if req.method == 'GET':
        uuid_time = int(time.time() * 1000)  # url 参数
        base_uuid_url = 'https://login.wx.qq.com/jslogin?appid=wx782c26e4c19acffb&redirect_uri=https%3A%2F%2Fwx.qq.com%2Fcgi-bin%2Fmmwebwx-bin%2Fwebwxnewloginpage&fun=new&lang=zh_CN&_={0}'
        uuid_url = base_uuid_url.format(uuid_time)
        res_uuid = requests.get(uuid_url)  # get QRcode uuid
        # result = re.findall('= "(.*)";', res_uuid.text)
        result = re.findall('= "(.*)";', res_uuid.text)
        print(result)
        uuid = result[0]

        # SET UUID UUID_TIME
        req.session['UUID_TIME'] = uuid_time
        req.session['UUID'] = uuid

        return render(req, 'login.html', {'uuid': uuid})


def check_login(req):
    response = {'code': 408, 'data': None}
    ctime = int(time.time() * 1000)
    base_login_url = 'https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid={0}&tip=0&r=-852639493&_={1}'
    login_url = base_login_url.format(req.session['UUID'], ctime)
    print(login_url)
    # http 长连接
    r1 = requests.get(login_url)
    print(login_url)
    if 'window.code=408' in r1.text:
        # not scan QR code 无人扫码
        response['code'] = 408
    elif 'window.code=201' in r1.text:
        # scan QR code avatar   获取头像
        response['code'] = 201
        response['data'] = re.findall("window.userAvatar = '(.*)';", r1.text)
    elif 'window.code=200' in r1.text:
        # scan QR code Login    扫码并确认登录
        req.session['LOGIN_COOKIE'] = r1.cookies.get_dict()  # get cookie
        base_redirect_url = re.findall('redirect_uri="(.*)";', r1.text)[0]
        redirect_url = base_redirect_url + '&fun=new&version=v2'

        # get Key 获取凭证
        r2 = requests.get(redirect_url)
        ticket_dict = ticket(r2.text)
        req.session['TICKED_DICT'] = ticket_dict
        # 保存登录成功之后的cookie
        req.session['TICKED_COOKIE'] = r2.cookies.get_dict()

        # 初始化,获取最近联系人信息 : 公众号
        post_data = {
            'BaseRequest': {
                'DeviceId': 'e807477778198957',
                'Sid': ticket_dict['wxsid'],
                'Uin': ticket_dict['wxuin'],
                'Skey': ticket_dict['skey'],
            }
        }
        print('ticket_dict', ticket_dict)

        # 用户初始化,讲最近联系人个人信息放在session中
        init_url = 'https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxinit?r=-855054718&pass_ticket={0}'.format(
            ticket_dict['pass_ticket'])

        r3 = requests.post(url=init_url,
                           json=post_data,
                           cookies=req.session['LOGIN_COOKIE'])
        print(r3)
        r3.encoding = 'utf-8'
        init_dict = json.loads(r3.text)
        print(init_dict)
        req.session['INIT_DICT'] = init_dict
        print(init_dict)
        response['code'] = 200

    return HttpResponse(json.dumps(response))


def avater(req):
    # https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgeticon?seq=1027154104&username=@69b1ae017d1a3a80137b3ec8e92975692391c4db64198fad6e58ed38e1c25423&skey=@crypt_d4c8b83f_8de6e98c87f37f8808d89867941598ff
    prev = req.GET.get('prev')
    print(prev)
    username = req.GET.get('username')
    skey = req.GET.get('skey')
    img_url = 'https://wx.qq.com{0}&username={1}&skey={2}'.format(prev, username, skey)
    print('imgurl', img_url)
    cookies = {}
    cookies.update(req.session['LOGIN_COOKIE'])
    cookies.update(req.session['TICKED_COOKIE'])
    res = requests.get(img_url, cookies=cookies, headers={'Content-Type': 'image/jpeg'})
    return HttpResponse(res.content)


def index(req):
    '''最近联系人'''
    return render(req, 'index.html')


def contact_list(req):
    '''
    all user  所有联系人
    :param req:
    :return:
    '''
    ctime = int(time.time() * 1000)
    # https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetcontact?lang=zh_CN&
    # pass_ticket=dQI61XpqPEgdhu7sHk0EDXwf6xOrh6Ngg1cGy8VGBrha3Hp8UYAPZ3GuQL0fs%252Bjd&r=1504101474930&seq=0&skey=@crypt_d4c8b83f_fed6558ea4f228430cfcd6ff2d2576c1
    base_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetcontact?lang=zh_CN&r={0}&seq=0&skey={1}&pass_ticket={2}"
    url = base_url.format(ctime, req.session['TICKED_DICT']['skey'], req.session['TICKED_DICT']['pass_ticket'])

    cookies = {}
    cookies.update(req.session['LOGIN_COOKIE'])
    cookies.update(req.session['TICKED_COOKIE'])
    r1 = requests.get(url, cookies=cookies)
    r1.encoding = 'utf8'

    user_list = json.loads(r1.text)

    return render(req, 'contact_list.html', {'user_list': user_list})


def send_msg(req):
    '''
    send msg 发送消息
    :param req:
    :return:
    '''
    current_user = req.session['INIT_DICT']['User']['UserName']
    to = req.POST.get('to')
    msg = req.POST.get('msg')

    ticket_dict = req.session['TICKED_DICT']
    ctime = int(time.time() * 1000)

    # 请求需要的参数
    post_data = {
        "BaseRequest": {
            "DeviceID": "e384757757885382",
            'Sid': ticket_dict['wxsid'],
            'Uin': ticket_dict['wxuin'],
            'Skey': ticket_dict['skey'],
        },
        "Msg": {
            "ClientMsgId": ctime,
            "LocalID": ctime,
            "FromUserName": current_user,
            "ToUserName": to,
            "Content": msg,
            "Type": 1
        },
        "Scene": 0
    }

    url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg?pass_ticket={0}".format(ticket_dict['pass_ticket'])

    res = requests.post(url=url,
                        data=json.dumps(post_data, ensure_ascii=False).encode('utf-8'),
                        headers={'Content-Type': 'application/json'})  # application/json,json.dumps(post_data)

    print(res.text)
    return HttpResponse('...')


def get_msg(req):
    '''
    get msg
    :param req:
    :return:
    '''
    # 检查是否有消息到来
    ctime = int(time.time() * 1000)

    ticket_dict = req.session['TICKED_DICT']
    check_msg_url = "https://webpush.wx.qq.com/cgi-bin/mmwebwx-bin/synccheck"

    cookies = {}
    cookies.update(req.session['LOGIN_COOKIE'])
    cookies.update(req.session['TICKED_COOKIE'])

    synckey_dict = req.session['INIT_DICT']['SyncKey']
    synckey_list = []

    for item in synckey_dict['List']:
        tmp = "%s_%s"%(item['Key'],item['Val'])
        synckey_list.append(tmp)

    synckey = '|'.join(synckey_list)

    r1 = requests.get(
        url=check_msg_url,
        params={
            'r': ctime,
            "deviceid": "e807477778198957",
            'sid': ticket_dict['wxsid'],
            'uin': ticket_dict['wxuin'],
            'skey': ticket_dict['skey'],
            '_': ctime,
            'synckey': synckey
        },
        cookies=cookies
    )
    print(r1.text)

    if '{retcode:"0",selector:"0"}' in r1.text:
        return HttpResponse('...')

    # 有消息,获取消息
    base_get_msg_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsync?sid={0}&skey={1}&lang=zh_CN&pass_ticket={2}"
    get_msg_url = base_get_msg_url.format(ticket_dict['wxsid'], ticket_dict['skey'], ticket_dict['pass_ticket'])

    post_data = {
        "BaseRequest": {
            "DeviceID": "e807477778198957",
            'Sid': ticket_dict['wxsid'],
            'Uin': ticket_dict['wxuin'],
            'Skey': ticket_dict['skey'],
        },
        'SyncKey': req.session['INIT_DICT']['SyncKey']
    }
    r2 = requests.post(
        url=get_msg_url,
        json=post_data,
        cookies=cookies
    )
    r2.encoding = 'utf-8'

    # 接收到消息,
    msg_dict = json.loads(r2.text)
    print(msg_dict)
    for msg in msg_dict['AddMsgList']:
        print('您有新消息到来：', msg['Content'],msg)
    init_dict = req.session['INIT_DICT']
    init_dict['SyncKey'] = msg_dict['SyncKey']
    req.session['INIT_DICT'] = init_dict

    return HttpResponse('...')